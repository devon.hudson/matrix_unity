﻿/*============================================================================*/
/*                           This file is part of:                            */
/*                               Matrix Unity                                 */
/*============================================================================*/
/* MIT License                                                                */
/*                                                                            */
/* Copyright (c) 2020 Devon Hudson                                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/*============================================================================*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Matrix;

public class MatrixClient : MonoBehaviour, IMatrixClient
{
    public Action<bool> LoginFinished { get; set; }
    public Action<bool> LogoutFinished { get; set; }
    public Action<SyncResponse> SyncFinished { get; set; }
    public Action<JoinedRoomsResponse> RetrievedJoinedRooms { get; set; }
    public Action<Tuple<RoomNameSearchParameters, RoomNameResponse>> RetrievedRoomName { get; set; }
    public Action<Tuple<RoomMessagesSearchParameters, RoomMessagesResponse>> RetrievedRoomMessages { get; set; }
    public Action<Tuple<DisplayNameSearchParameters, DisplayNameResponse>> RetrievedDisplayName { get; set; }

    private MatrixClientInstance clientInstance = null;

    private bool teardownEdgeTrigger = false;
    private int transactionID = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    void Update()
    {
        if(teardownEdgeTrigger)
        {
            if(clientInstance != null)
            {
                ClearEventConnections();
                clientInstance.StopSyncPoll();
                clientInstance.StopAllCoroutines();
                Destroy(clientInstance);
            }
            teardownEdgeTrigger = false;
        }
    }

    void OnDestroy()
    {

    }

    bool IMatrixClient.IsLoggedIn()
    {
        return (clientInstance != null) ? clientInstance.IsLoggedIn() : false;
    }

    void IMatrixClient.Login(LoginParameters parameters)
    {
        if(clientInstance == null)
        {
            Debug.Log("Creating new Matrix Client Instance");
            clientInstance = gameObject.AddComponent<MatrixClientInstance>();
            SetupEventConnections();
        }

        if(clientInstance && !clientInstance.IsLoggedIn())
        {
            clientInstance.cachedState.homeserver = parameters.homeserver;
            LoginData loginData = new LoginData() {
                type = ProtocolConstants.MATRIX_DEFAULT_LOGIN_FLOW,
                user = parameters.username,
                password = parameters.password
            };

            string loginJson = SerializationUtility.Serialize(loginData);
            clientInstance.SendHTTPRequest(HTTPMethod.POST, RequestType.LOGIN, clientInstance.cachedState.homeserver + ProtocolConstants.MATRIX_CLIENT_BASE_PATH +
                ProtocolConstants.MATRIX_LOGIN_PATH, new POSTParameters { data = loginJson });
        }
    }

    void IMatrixClient.Logout()
    {
        if(clientInstance == null || !clientInstance.IsLoggedIn())
        {
            Debug.Log("Write this log here");
            Debug.LogError("Called Logout without having valid matrix client logged in. Try logging in first.");
            return;
        }

        clientInstance.SendHTTPRequest(HTTPMethod.POST, RequestType.LOGOUT, clientInstance.cachedState.homeserver + ProtocolConstants.MATRIX_CLIENT_BASE_PATH +
            ProtocolConstants.MATRIX_LOGOUT_PATH);
    }

    void IMatrixClient.StartSyncPoll(uint timeoutSeconds)
    {
        if(clientInstance == null || !clientInstance.IsLoggedIn())
        {
            Debug.LogError("Called StartSyncPoll without having valid matrix client logged in. Try logging in first.");
            return;
        }

        clientInstance.StartSyncPoll(timeoutSeconds);
    }

    void IMatrixClient.StopSyncPoll()
    {
        if(clientInstance == null || !clientInstance.IsLoggedIn())
        {
            Debug.LogError("Called StopSyncPoll without having valid matrix client logged in. Try logging in first.");
            return;
        }

        clientInstance.StopSyncPoll();
    }

    void IMatrixClient.GetJoinedRooms()
    {
        if(clientInstance == null || !clientInstance.IsLoggedIn())
        {
            Debug.LogError("Called GetJoinedRooms without having valid matrix client logged in. Try logging in first.");
            return;
        }

        clientInstance.SendHTTPRequest(HTTPMethod.GET, RequestType.JOINED_ROOMS, clientInstance.cachedState.homeserver + ProtocolConstants.MATRIX_CLIENT_BASE_PATH +
            ProtocolConstants.MATRIX_JOINED_ROOMS);
    }

    void IMatrixClient.GetRoomName(RoomNameSearchParameters parameters)
    {
        if(clientInstance == null || !clientInstance.IsLoggedIn())
        {
            Debug.LogError("Called GetRoomName without having valid matrix client logged in. Try logging in first.");
            return;
        }

        clientInstance.SendHTTPRequest(HTTPMethod.GET, RequestType.ROOM_NAME, clientInstance.cachedState.homeserver + ProtocolConstants.MATRIX_CLIENT_BASE_PATH +
            ProtocolConstants.MATRIX_ROOMS_PATH + "/" + parameters.roomID + "/" + ProtocolConstants.MATRIX_EVENT_TYPE_STATE + "/" +
            ProtocolConstants.MATRIX_ROOM_STATE_KEY_NAME, parameters);
    }

    void IMatrixClient.GetUserDisplayName(DisplayNameSearchParameters parameters)
    {
        if(clientInstance == null || !clientInstance.IsLoggedIn())
        {
            Debug.LogError("Called GetUserDisplayName without having valid matrix client logged in. Try logging in first.");
            return;
        }

        clientInstance.SendHTTPRequest(HTTPMethod.GET, RequestType.DISPLAY_NAME, clientInstance.cachedState.homeserver + ProtocolConstants.MATRIX_CLIENT_BASE_PATH +
            ProtocolConstants.MATRIX_PROFILE_PATH + "/" + parameters.userID + ProtocolConstants.MATRIX_PROFILE_DISPLAY_NAME, parameters);
    }

    void IMatrixClient.GetRoomMessages(RoomMessagesSearchParameters parameters)
    {
        if(clientInstance == null || !clientInstance.IsLoggedIn())
        {
            Debug.LogError("Called GetRoomMessages without having valid matrix client logged in. Try logging in first.");
            return;
        }

        string query = "?dir=";
        switch(parameters.searchDirection)
        {
            case SearchDirection.BACKWARD:
                query += "b";
                if(clientInstance.cachedState.lastMessageBack != "")
                {
                    query += "&from=" + clientInstance.cachedState.lastMessageBack;
                }
                break;

            case SearchDirection.FORWARD:
                query += "f";
                if(clientInstance.cachedState.lastMessageForward != "")
                {
                    query += "&from=" + clientInstance.cachedState.lastMessageForward;
                }
                break;

            default:
                Debug.LogError("Invalid search direction provided");
                return;
        }

        clientInstance.SendHTTPRequest(HTTPMethod.GET, RequestType.ROOM_MESSAGES, clientInstance.cachedState.homeserver + ProtocolConstants.MATRIX_CLIENT_BASE_PATH +
            ProtocolConstants.MATRIX_ROOMS_PATH + "/" + parameters.roomID + ProtocolConstants.MATRIX_ROOM_MESSAGES + query, parameters);
    }

    void IMatrixClient.PostRoomMessage(string roomID, string message)
    {
        if(clientInstance == null || !clientInstance.IsLoggedIn())
        {
            Debug.LogError("Called PostRoomMessage without having valid matrix client logged in. Try logging in first.");
            return;
        }

        string endpoint = clientInstance.cachedState.homeserver + ProtocolConstants.MATRIX_CLIENT_BASE_PATH + ProtocolConstants.MATRIX_ROOMS_PATH + "/" + roomID +
            ProtocolConstants.MATRIX_SEND_PATH + "/" + ProtocolConstants.MATRIX_ROOM_EVENT_KEY_MESSAGE + "/" + System.Threading.Interlocked.Increment(ref transactionID);

        MessageContent content = new MessageContent {
            body = message,
            msgtype = ProtocolConstants.MATRIX_MESSAGE_TYPE_TEXT
        };

        clientInstance.SendHTTPRequest(HTTPMethod.PUT, RequestType.SEND_MESSAGE_EVENT_TO_ROOM, endpoint, new PUTParameters { data = SerializationUtility.Serialize(content) });
    }

    private void HandleLogoutFinished(bool success)
    {
        if(success)
        {
            teardownEdgeTrigger = true;
        }
        LogoutFinished?.Invoke(success);
    }

    private void SetupEventConnections()
    {
        clientInstance.LoginFinished += LoginFinished;
        clientInstance.LogoutFinished += HandleLogoutFinished;
        clientInstance.SyncFinished += SyncFinished;
        clientInstance.RetrievedJoinedRooms += RetrievedJoinedRooms;
        clientInstance.RetrievedRoomName += RetrievedRoomName;
        clientInstance.RetrievedRoomMessages += RetrievedRoomMessages;
        clientInstance.RetrievedDisplayName += RetrievedDisplayName;
    }

    private void ClearEventConnections()
    {
        clientInstance.LoginFinished -= LoginFinished;
        clientInstance.LogoutFinished -= HandleLogoutFinished;
        clientInstance.SyncFinished -= SyncFinished;
        clientInstance.RetrievedJoinedRooms -= RetrievedJoinedRooms;
        clientInstance.RetrievedRoomName -= RetrievedRoomName;
        clientInstance.RetrievedRoomMessages -= RetrievedRoomMessages;
        clientInstance.RetrievedDisplayName -= RetrievedDisplayName;
    }
}

