/*============================================================================*/
/*                           This file is part of:                            */
/*                               Matrix Unity                                 */
/*============================================================================*/
/* MIT License                                                                */
/*                                                                            */
/* Copyright (c) 2020 Devon Hudson                                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/*============================================================================*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Matrix
{
    public enum HTTPMethod
    {
        GET,
        PUT,
        POST,
        DELETE
    }

    public enum RequestType
    {
        SYNC,
        LOGIN,
        LOGOUT,
        JOINED_ROOMS,
        ROOM_NAME,
        ROOM_MESSAGES,
        DISPLAY_NAME,
        SEND_MESSAGE_EVENT_TO_ROOM
    }

    public class POSTParameters : IInputParameters
    {
        public string data = "";
    }

    public class PUTParameters : IInputParameters
    {
        public string data = "";
    }

    public class MatrixClientInstance : MonoBehaviour
    {
        public Action<bool> LoginFinished { get; set; }
        public Action<bool> LogoutFinished { get; set; }
        public Action<SyncResponse> SyncFinished { get; set; }
        public Action<JoinedRoomsResponse> RetrievedJoinedRooms { get; set; }
        public Action<Tuple<RoomNameSearchParameters, RoomNameResponse>> RetrievedRoomName { get; set; }
        public Action<Tuple<RoomMessagesSearchParameters, RoomMessagesResponse>> RetrievedRoomMessages { get; set; }
        public Action<Tuple<DisplayNameSearchParameters, DisplayNameResponse>> RetrievedDisplayName { get; set; }

        public CachedState cachedState = new CachedState();

        private readonly object loggedInLock = new object();
        private volatile bool isLoggedIn = false;

        private bool stopClient = false;
        private uint syncRetryBackoffCount = 0;

        private const int WEB_REQUEST_TIMEOUT_S = 60;
        private const int DELAY_TIME_BEFORE_FIRST_SYNC_S = 5;
        private const int MAX_DELAY_CYCLES = 10;

        private Coroutine SyncPollingCoroutine = null;

        private List<RequestType> RequestTypesRequiringInitialSync = new List<RequestType> {
            RequestType.ROOM_MESSAGES
        };

        private List<RequestType> RequestTypesToPerformSynchronously = new List<RequestType> {
            RequestType.ROOM_MESSAGES
        };

        private List<RequestType> CurrentlyRunningRequests = new List<RequestType>();

        public bool IsLoggedIn()
        {
            lock(loggedInLock)
            {
                return isLoggedIn;
            }
        }

        public void SendHTTPRequest(HTTPMethod method, RequestType type, string uri, IInputParameters optionalParameters = null)
        {
            switch(method)
            {
                case HTTPMethod.GET:
                    StartCoroutine(SendHTTPGetRequest(type, uri, optionalParameters));
                    break;

                case HTTPMethod.POST:
                    StartCoroutine(SendHTTPPostRequest(type, uri, (POSTParameters)optionalParameters));
                    break;

                case HTTPMethod.PUT:
                    StartCoroutine(SendHTTPPutRequest(type, uri, (PUTParameters)optionalParameters));
                    break;

                default:
                    Debug.LogError("HTTPMethod type unknown: " + method);
                    break;
            }
        }

        public void StartSyncPoll(uint timeoutSeconds)
        {
            if(SyncPollingCoroutine == null)
            {
                SyncPollingCoroutine = StartCoroutine(PollSync(timeoutSeconds));
            }
            else
            {
                Debug.LogError("Not starting sync while previous sync is still running.");
            }
        }

        public void StopSyncPoll()
        {
            stopClient = true;
            StopCoroutine(SyncPollingCoroutine);
        }

        private IEnumerator PollSync(uint timeoutSeconds)
        {
            while(!stopClient && isLoggedIn)
            {
                string query = "?timeout=" + timeoutSeconds * 1000;
                if(cachedState.lastSync != "")
                {
                    query += "&since=" + cachedState.lastSync;
                }
                yield return SendHTTPGetRequest(RequestType.SYNC, cachedState.homeserver + ProtocolConstants.MATRIX_CLIENT_BASE_PATH +
                    ProtocolConstants.MATRIX_SYNC_PATH + query);

                if(syncRetryBackoffCount != 0)
                {
                    long syncTimeout = (long)Math.Pow(2, syncRetryBackoffCount) - 1;
                    yield return new WaitForSecondsRealtime(syncTimeout);
                }
            }
            Debug.Log("Finished sync sequence");
            SyncPollingCoroutine = null;

            yield return null;
        }

        // TODO : Refactor all these HTTP Request functions to remove duplication
        private IEnumerator SendHTTPGetRequest(RequestType type, string uri, IInputParameters optionalParameters = null)
        {
            if(RequestTypesToPerformSynchronously.Contains(type) && CurrentlyRunningRequests.Contains(type))
            {
                Debug.LogWarning("Request of type " + type + " is already running.");
                yield break;
            }

            CurrentlyRunningRequests.Add(type);
            int retryCount = 0;
            if(RequestTypesRequiringInitialSync.Contains(type))
            {
                while(cachedState.lastSync.Length == 0 && retryCount++ < MAX_DELAY_CYCLES)
                {
                    yield return new WaitForSeconds(DELAY_TIME_BEFORE_FIRST_SYNC_S);
                }
            }

            if(retryCount >= MAX_DELAY_CYCLES)
            {
                Debug.LogError("First sync hasn't occurred within " + MAX_DELAY_CYCLES * DELAY_TIME_BEFORE_FIRST_SYNC_S + " seconds. Giving up on request.");
                yield break;
            }

            using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
            {
                if(cachedState.accessToken.Length != 0)
                {
                    // Request and wait for the desired page.
                    webRequest.SetRequestHeader("Authorization", "Bearer " + cachedState.accessToken);
                    webRequest.timeout = WEB_REQUEST_TIMEOUT_S;
                    yield return webRequest.SendWebRequest();

                    if (webRequest.isNetworkError || webRequest.isHttpError)
                    {
                        Debug.Log("Error: " + webRequest.error);
                        HandleHTTPRequestFailed(type, webRequest, optionalParameters);
                    }
                    else
                    {
                        //Debug.Log("Received:\n" + webRequest.downloadHandler.text);
                        HandleHTTPRequestSuccess(type, webRequest, optionalParameters);
                    }
                }
                else
                {
                    Debug.LogWarning("No access token acquired... Not performing GET for " + type.ToString());
                }
            }

            CurrentlyRunningRequests.Remove(type);

            yield return null;
        }

        private IEnumerator SendHTTPPostRequest(RequestType type, string uri, POSTParameters optionalParameters = null)
        {
            string data = (optionalParameters != null) ? optionalParameters.data : "";
            using (UnityWebRequest webRequest = UnityWebRequest.Post(uri, data))
            {
                // Request and wait for the desired page.
                if(data.Length != 0)
                {
                    byte[] encodedPayload = new System.Text.UTF8Encoding().GetBytes(data);
                    webRequest.uploadHandler = (UploadHandler) new UploadHandlerRaw(encodedPayload);
                    webRequest.SetRequestHeader("Content-Type", ProtocolConstants.JSON_CONTENT_TYPE);
                    webRequest.SetRequestHeader("cache-control", "no-cache");
                }
                if(cachedState.accessToken.Length != 0)
                {
                    webRequest.SetRequestHeader("Authorization", "Bearer " + cachedState.accessToken);
                }
                webRequest.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
                webRequest.timeout = WEB_REQUEST_TIMEOUT_S;

                yield return webRequest.SendWebRequest();

                if (webRequest.isNetworkError || webRequest.isHttpError)
                {
                    Debug.Log("Error: " + webRequest.error);
                    HandleHTTPRequestFailed(type, webRequest);
                }
                else
                {
                    //Debug.Log("Received:\n" + webRequest.downloadHandler.text);
                    HandleHTTPRequestSuccess(type, webRequest);
                }
            }

            yield return null;
        }

        private IEnumerator SendHTTPPutRequest(RequestType type, string uri, PUTParameters optionalParameters = null)
        {
            string data = (optionalParameters != null) ? optionalParameters.data : "";
            using (UnityWebRequest webRequest = UnityWebRequest.Put(uri, data))
            {
                // Request and wait for the desired page.
                if(data.Length != 0)
                {
                    byte[] encodedPayload = new System.Text.UTF8Encoding().GetBytes(data);
                    webRequest.uploadHandler = (UploadHandler) new UploadHandlerRaw(encodedPayload);
                    webRequest.SetRequestHeader("Content-Type", ProtocolConstants.JSON_CONTENT_TYPE);
                    webRequest.SetRequestHeader("cache-control", "no-cache");
                }
                if(cachedState.accessToken.Length != 0)
                {
                    webRequest.SetRequestHeader("Authorization", "Bearer " + cachedState.accessToken);
                }
                webRequest.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
                webRequest.timeout = WEB_REQUEST_TIMEOUT_S;

                Debug.LogWarning(webRequest.ToString());

                yield return webRequest.SendWebRequest();

                if (webRequest.isNetworkError || webRequest.isHttpError)
                {
                    Debug.Log("Error: " + webRequest.error);
                    HandleHTTPRequestFailed(type, webRequest);
                }
                else
                {
                    //Debug.Log("Received:\n" + webRequest.downloadHandler.text);
                    HandleHTTPRequestSuccess(type, webRequest);
                }
            }

            yield return null;
        }

        private void HandleHTTPRequestFailed(RequestType type, UnityWebRequest request, IInputParameters optionalParameters = null)
        {
            switch(type)
            {
                case RequestType.LOGIN:
                    Debug.LogWarning("Login finished unsuccessfully");
                    LoginFinished?.Invoke(false);
                    break;

                case RequestType.LOGOUT:
                    Debug.LogWarning("Logout finished unsuccessfully");
                    LogoutFinished?.Invoke(false);
                    break;

                case RequestType.SYNC:
                    Debug.LogWarning("Sync finished unsuccessfully");
                    if(request.responseCode == 429)
                    {
                        Debug.LogWarning("Sending too many sync requests.");
                        syncRetryBackoffCount++;
                    }
                    break;

                case RequestType.JOINED_ROOMS:
                    Debug.LogWarning("Failed getting list of joined rooms");
                    break;

                case RequestType.ROOM_NAME:
                    Debug.LogWarning("Failed to obtain room name.");
                    break;

                case RequestType.ROOM_MESSAGES:
                    Debug.LogWarning("Failed to obtain room messages.");
                    break;

                case RequestType.DISPLAY_NAME:
                    Debug.LogWarning("Failed to obtain display name.");
                    break;

                case RequestType.SEND_MESSAGE_EVENT_TO_ROOM:
                    Debug.LogWarning("Unsuccessfully sent message to room");
                    break;

                default:
                    Debug.LogError("Unknown Get request type failed.");
                    break;
            }
        }

        private void HandleHTTPRequestSuccess(RequestType type, UnityWebRequest request, IInputParameters optionalParameters = null)
        {
            switch(type)
            {
                case RequestType.LOGIN:
                    LoginResponse loginResponse = SerializationUtility.Deserialize<LoginResponse>(request.downloadHandler.text);
                    if(loginResponse != null)
                    {
                        lock(loggedInLock)
                        {
                            this.isLoggedIn = true;
                        }
                        cachedState.accessToken = loginResponse.access_token;
                        LoginFinished?.Invoke(true);
                    }
                    break;

                case RequestType.LOGOUT:
                    this.cachedState.Clear();
                    lock(loggedInLock)
                    {
                        isLoggedIn = false;
                    }
                    LogoutFinished?.Invoke(true);
                    break;

                case RequestType.SYNC:
                    syncRetryBackoffCount = 0;
                    SyncResponse syncResponse = SerializationUtility.Deserialize<SyncResponse>(request.downloadHandler.text);
                    if(syncResponse != null)
                    {
                        var syncJson = SimpleJSON.JSON.Parse(request.downloadHandler.text);

                        foreach(KeyValuePair<string, SimpleJSON.JSONNode> jsonRoom in syncJson["rooms"]["join"].AsObject)
                        {
                            int syncEventIndex = 0;
                            foreach(var jsonEvent in jsonRoom.Value["timeline"]["events"].Values)
                            {
                                if(jsonEvent["type"] == ProtocolConstants.MATRIX_ROOM_EVENT_KEY_MESSAGE)
                                {
                                    var messageContent = jsonEvent["content"];
                                    if(messageContent["msgtype"].Value == ProtocolConstants.MATRIX_MESSAGE_TYPE_TEXT)
                                    {
                                        TextMessageContent textMessage = SerializationUtility.Deserialize<TextMessageContent>(messageContent.ToString());
                                        syncResponse.rooms.join[jsonRoom.Key].timeline.events[syncEventIndex].content = textMessage;
                                    }
                                }

                                syncEventIndex++;
                            }
                        }

                        cachedState.lastSync = syncResponse.next_batch;
                        SyncFinished?.Invoke(syncResponse);
                    }
                    break;

                case RequestType.JOINED_ROOMS:
                    JoinedRoomsResponse joinedRooms = SerializationUtility.Deserialize<JoinedRoomsResponse>(request.downloadHandler.text);
                    if(joinedRooms != null)
                    {
                        RetrievedJoinedRooms?.Invoke(joinedRooms);
                    }
                    break;

                case RequestType.ROOM_NAME:
                    RoomNameSearchParameters roomNameSearchParameters = optionalParameters as RoomNameSearchParameters;
                    if(roomNameSearchParameters == null)
                    {
                        Debug.LogError("Parameters don't align with Request type");
                        break;
                    }

                    RoomNameResponse roomName = SerializationUtility.Deserialize<RoomNameResponse>(request.downloadHandler.text);
                    if(roomName != null)
                    {
                        RetrievedRoomName?.Invoke(new Tuple<RoomNameSearchParameters, RoomNameResponse>(roomNameSearchParameters, roomName));
                    }
                    break;

                case RequestType.ROOM_MESSAGES:
                    RoomMessagesSearchParameters roomMessagesSearchParameters = optionalParameters as RoomMessagesSearchParameters;
                    if(roomMessagesSearchParameters == null)
                    {
                        Debug.LogError("Parameters don't align with Request type");
                        break;
                    }

                    RoomMessagesResponse roomMessages = SerializationUtility.Deserialize<RoomMessagesResponse>(request.downloadHandler.text);

                    if(roomMessages != null)
                    {
                        var messagesJson = SimpleJSON.JSON.Parse(request.downloadHandler.text);

                        int messagesEventIndex = 0;
                        foreach(var jsonEvent in messagesJson["chunk"].Values)
                        {
                            var messageContent = jsonEvent["content"];
                            if(messageContent["msgtype"].Value == ProtocolConstants.MATRIX_MESSAGE_TYPE_TEXT)
                            {
                                TextMessageContent textMessage = SerializationUtility.Deserialize<TextMessageContent>(messageContent.ToString());
                                roomMessages.chunk[messagesEventIndex].content = textMessage;
                            }

                            messagesEventIndex++;
                        }

                        switch(roomMessagesSearchParameters.searchDirection)
                        {
                            case SearchDirection.BACKWARD:
                                cachedState.lastMessageBack = roomMessages.end;
                                break;
                            case SearchDirection.FORWARD:
                                cachedState.lastMessageForward = roomMessages.end;
                                break;
                            default:
                                Debug.LogError("Invalid search direction provided");
                                return;
                        }
                        RetrievedRoomMessages?.Invoke(new Tuple<RoomMessagesSearchParameters, RoomMessagesResponse>(roomMessagesSearchParameters, roomMessages));
                    }
                    break;

                case RequestType.DISPLAY_NAME:
                    DisplayNameSearchParameters displayNameSearchParameters = optionalParameters as DisplayNameSearchParameters;
                    if(displayNameSearchParameters == null)
                    {
                        Debug.LogError("Parameters don't align with Request type");
                        break;
                    }

                    DisplayNameResponse displayName = SerializationUtility.Deserialize<DisplayNameResponse>(request.downloadHandler.text);
                    if(displayName != null)
                    {
                        RetrievedDisplayName?.Invoke(new Tuple<DisplayNameSearchParameters, DisplayNameResponse>(displayNameSearchParameters, displayName));
                    }
                    break;

                case RequestType.SEND_MESSAGE_EVENT_TO_ROOM:
                    Debug.Log("Successfully sent message to room");
                    // LoginResponse loginResponse = SerializationUtility.Deserialize<LoginResponse>(request.downloadHandler.text);
                    // if(loginResponse != null)
                    // {
                    //     isLoggedIn = true;
                    //     cachedState.accessToken = loginResponse.access_token;
                    //     LoginFinished?.Invoke(true);
                    // }
                    break;

                default:
                    Debug.LogError("Unknown Get request type succeeded.");
                    break;
            }
        }
    }
}
