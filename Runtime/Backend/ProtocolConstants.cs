/*============================================================================*/
/*                           This file is part of:                            */
/*                               Matrix Unity                                 */
/*============================================================================*/
/* MIT License                                                                */
/*                                                                            */
/* Copyright (c) 2020 Devon Hudson                                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/*============================================================================*/

namespace Matrix
{
    public static class ProtocolConstants
    {
        public static readonly string JSON_CONTENT_TYPE = "application/json";
        public static readonly string LOGIN_TYPE_KEY = "type";
        public static readonly string LOGIN_FLOWS_KEY = "flows";
        public static readonly string LOGIN_USERNAME_KEY = "user";
        public static readonly string LOGIN_PASSWORD_KEY = "password";
        public static readonly string HOMESERVER_KEY = "m.homeserver";
        public static readonly string IDENTITY_SERVER_KEY = "m.identity_server";
        public static readonly string BASE_URL_KEY = "base_url";
        public static readonly string MATRIX_DEFAULT_LOGIN_FLOW = "m.login.password";
        public static readonly string MATRIX_ROOM_STATE_KEY_NAME = "m.room.name";
        public static readonly string MATRIX_ROOM_EVENT_KEY_MESSAGE = "m.room.message";
        public static readonly string MATRIX_ROOM_EVENT_KEY_MEMBER = "m.room.member";
        public static readonly string MATRIX_MESSAGE_TYPE_TEXT = "m.text";
        public static readonly string MATRIX_EVENT_TYPE_STATE = "state";
        public static readonly string MATRIX_ACCESS_TOKEN_KEY = "access_token";
        public static readonly string MATRIX_WELL_KNOWN = "/.well-known/matrix/client";
        public static readonly string MATRIX_API_VERSIONS = "/_matrix/client/versions";
        public static readonly string MATRIX_CLIENT_BASE_PATH = "/_matrix/client/r0";
        public static readonly string MATRIX_LOGIN_PATH = "/login";
        public static readonly string MATRIX_LOGOUT_PATH = "/logout";
        public static readonly string MATRIX_SYNC_PATH = "/sync";
        public static readonly string MATRIX_ROOMS_PATH = "/rooms";
        public static readonly string MATRIX_SEND_PATH = "/send";
        public static readonly string MATRIX_PROFILE_PATH = "/profile";
        public static readonly string MATRIX_ROOM_MESSAGES = "/messages";
        public static readonly string MATRIX_PROFILE_DISPLAY_NAME = "/displayname";
        public static readonly string MATRIX_JOINED_ROOMS = "/joined_rooms";
    }
}
