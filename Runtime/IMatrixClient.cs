/*============================================================================*/
/*                           This file is part of:                            */
/*                               Matrix Unity                                 */
/*============================================================================*/
/* MIT License                                                                */
/*                                                                            */
/* Copyright (c) 2020 Devon Hudson                                            */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/*============================================================================*/

using System;

namespace Matrix
{
    public enum SearchDirection
    {
        BACKWARD,
        FORWARD
    }

    public interface IInputParameters {}

    public class LoginParameters : IInputParameters
    {
        public string username;
        public string password;
        public string homeserver;
    }

    public class RoomNameSearchParameters : IInputParameters
    {
        public string roomID;
    }

    public class RoomMessagesSearchParameters : IInputParameters
    {
        public string roomID;
        public SearchDirection searchDirection;
    }

    public class DisplayNameSearchParameters : IInputParameters
    {
        public string userID;
    }

    public interface IMatrixClient
    {
        Action<bool> LoginFinished { get; set; }
        Action<bool> LogoutFinished { get; set; }
        Action<SyncResponse> SyncFinished { get; set; }
        Action<JoinedRoomsResponse> RetrievedJoinedRooms { get; set; }
        Action<Tuple<RoomNameSearchParameters, RoomNameResponse>> RetrievedRoomName { get; set; }
        Action<Tuple<RoomMessagesSearchParameters, RoomMessagesResponse>> RetrievedRoomMessages { get; set; }
        Action<Tuple<DisplayNameSearchParameters, DisplayNameResponse>> RetrievedDisplayName { get; set; }

        void Login(LoginParameters parameters);
        void Logout();
        bool IsLoggedIn();

        void StartSyncPoll(uint timeoutSeconds);
        void StopSyncPoll();

        void GetJoinedRooms();
        void GetRoomName(RoomNameSearchParameters parameters);

        void GetUserDisplayName(DisplayNameSearchParameters parameters);

        void GetRoomMessages(RoomMessagesSearchParameters parameters);
        void PostRoomMessage(string roomID, string message);
    }
}
