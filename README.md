# matrix-unity

Matrix Unity is a [Matrix](https://matrix.org/) client library built to work natively with the Unity Game Engine. 
All code is self contained with no external dependencies other than the Unity Game Engine itself.

## Demo Project
->> [Click Here](https://gitlab.com/devon.hudson/matrix-unity-demo) <<- For an example project using Matrix Unity to create a simple Matrix chat client.
